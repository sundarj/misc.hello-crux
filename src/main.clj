(ns main
  (:require
   [clojure.java.io]
   [crux.api :as crux]))


(defn start-crux! []
  (letfn [(kv-store [dir]
            {:kv-store {:crux/module 'crux.rocksdb/->kv-store
                        :db-dir (clojure.java.io/file dir)
                        :sync? true}})]
    (crux/start-node
     {:crux/tx-log (kv-store "data/dev/tx-log")
      :crux/document-store (kv-store "data/dev/doc-store")
      :crux/index-store (kv-store "data/dev/index-store")})))


(def crux-node (start-crux!))


(defn stop-crux! []
  (.close crux-node))


(comment
  (start-crux!)
  (stop-crux!)
  (crux/submit-tx crux-node [[:crux.tx/put
                              {:crux.db/id "hi2u"
                               :user/name "zig"}]])
  (crux/q (crux/db crux-node) '{:find [e]
                                :where [[e :user/name "zig"]]})
  )

